# frozen_string_literal: true

# This file was generated programmatically on 19-APR-2022 using the `package_gem` functionality of mmlite.

Gem::Specification.new do |s|
  s.name        = "lodepath"
  s.version     = "0.2.3"
  s.authors     = ["Michael Faughn"]
  s.email       = ["michael.faughn@nist.gov"]
  s.homepage    = "https://gitlab.com/prometheuscomputing/lodepath"
  s.summary     = "Commonly used functionality for manipulating $LOAD_PATH"
  s.description = "Commonly used functionality for manipulating $LOAD_PATH (and opening installed gems in an editor)."
  s.licenses    = ['MPL-2.0']
  s.executables = [
      "lodepath"
  ]
  s.files = [
      "LICENSE",
      "bin/lodepath",
      "lib/lodepath.rb",
      "lib/lodepath/git_warning.rb",
      "lib/lodepath/lodepath.rb",
      "lib/lodepath/meta_info.rb",
      "lib/lodepath/find.rb",
      "lib/lodepath/relative.rb"
  ]
  s.test_files = [
      "spec/spec_helper.rb",
      "spec/lodepath_spec.rb",
      "spec/test_data/custom_load_path.yml",
      "spec/test_data/do_not_move_this_folder.txt",
      "test_data/lodepath.yaml",
      "test_data/do_not_move_this_folder.txt"
  ]
  s.add_dependency("rainbow")
  s.add_development_dependency("rspec", ">= 3")
end
