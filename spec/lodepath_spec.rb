require 'spec_helper'
# TODO add tests for functionality in relative.rb

# Simple enough now that you should just look at it and make sure it is OK
describe "LodePath functionality" do
  
  it "given a partial path for a file, it finds the file on the load path and returns the absolute path" do
    expect(LodePath.find('lodepath/meta_info.rb')).to eq(File.expand_path(File.join(__dir__, '../lib/lodepath/meta_info.rb')))
  end
  
  it "given a partial path for a directory, it finds the directory on the load path and returns the absolute path" do
    expect(LodePath.find('lodepath', :dir)).to eq(File.expand_path(File.join(__dir__, '../lib/lodepath')))
  end
  
  it "given a partial path for a directory that is not on the LodePath, it causes an exception" do
    expect {LodePath.find('bin', :dir)}.to raise_exception(LodePath::Error)
  end
  
  it "adds the calling project's lib folder to $LOAD_PATH" do
    remove_lib
    path = File.expand_path('~/projects/lodepath/lib')
    expect($:.include?(path)).to be false
    LodePath.add_project_lib
    expect($:.include?(path)).to be true
  end
  
  it "read configuration from a yaml file and adjust the loadpath" do
    LodePath.amend
    user_path = File.expand_path('~')
    paths_to_test = $:.collect do |path|
      path.sub(user_path, '')
    end
    expect(paths_to_test).to include('/fake_projects_folder/custom_project_location')
    expect(paths_to_test).to include('/projects/fake_project')
    expect(paths_to_test).to include('/projects/sequel_specific_associations/lib')
    expect { LodePath.display(true) }.to output(/\.(rvm|rbenv)/).to_stdout
    expect { LodePath.display(true) }.to output(/Non-existant paths in.+red.+Added paths in.+green/).to_stdout
    expect { LodePath.display }.to output(/\$LOAD_PATH amendments:/).to_stdout
    expect { LodePath.display }.to output(/dir not found/).to_stdout
  end
end

