require 'rainbow'
require 'rspec'
require_relative '../lib/lodepath.rb'

def remove_lib
  $:.delete_if { |e| e =~ /\/lodepath/ }
  # puts $:
end