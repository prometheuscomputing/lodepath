require 'rainbow'
require 'yaml'
require 'find'
require 'pathname'

module LodePath
  
  def self.<<(path)
    $:.unshift(path)
  end
  
  def self.add_project_lib
    called_location = caller.first.gsub(/:.*/, '')
    proj_dir = find_project_dir(called_location)
    raise "Can't find project directory around #{called_location}" unless proj_dir
    LodePath.<< File.join(proj_dir, 'lib')
  end
    
  # One or more pathfiles may be sent
  # Note that this method may be called more than once and the amendments will be cumulative
  def self.amend(pathfiles = [], opts = {})
    pathfiles = [pathfiles].flatten
    if pathfiles.empty?
      called_location = opts[:from] || caller.first.gsub(/:.*/, '')
      root_path = find_project_dir(called_location)
      # puts Rainbow(root_path).blue
      Find.find(root_path) do |f|
        # These files are intended to be common to everyone's usage.  Example need for this is SSA's usage of local directories during rspec tests (everyone runs the tests this way.)
        # not picky about spelling.  Acceptable filenames are loadpath.yaml, lodepath.yaml, lode_path.yaml, load_path.yaml. .yml extenstion also works.
        pathfiles << f if f =~ /\/(?:lode|load)_?path.ya?ml$/
      end
      Find.find(root_path) do |f|
        # These files are intended to be used only in an individual developer's environment and an appropriate entry should be added into .git_ignore in order to ensure that these files are not commited to any repositories.  The 
        # These files are named the same as above but have some characters prepended, e.g. my_lodepath.yaml, customload_path.yml, etc..
        pathfiles << f if f =~ /\w\w*(?:lode|load)_?path.ya?ml$/
      end
    end
    pathfiles.each do |pf|
      amendments = YAML.load(File.open(pf))
      next unless amendments
      amendments.each do |path, entries|        
        next unless entries   
        entries.each do |e|
          raw_path = File.expand_path(File.join(path, e))
          lib_path = File.join(raw_path, 'lib')
          if Pathname.new(lib_path).exist?
            $:.unshift(lib_path)
          else
            $:.unshift(raw_path)
          end
        end
      end
    end    
  end
  
  def self.display(verbose = false)
    regex = /\.?((rvm\/(gems|rubies))|(rbenv\/versions))|homebrew/
    if verbose
      puts; puts Rainbow('$LOAD_PATH').magenta
      puts Rainbow('Non-existant paths in ').magenta + Rainbow('red').red + Rainbow('. Added paths in ').magenta + Rainbow('green').green + Rainbow(':').magenta
      $:.each do |path|
        if path =~ regex
          puts '  ' + path
          next
        end
        if File.exist?(path)
          puts '  ' + Rainbow(path).green
        else
          puts '  ' + Rainbow(path).red
        end
      end
      return
    end
    puts Rainbow("\n$LOAD_PATH amendments:").magenta
    found = $:.select do |path|
      next if path =~ regex
      if File.exist?(path)
        green_path = path.gsub(/(\w+)\/lib/, Rainbow('\1').green + '/lib')
        puts '  ' + green_path
      else
        puts '  ' + Rainbow(path).red + Rainbow(' (dir not found)').yellow
      end
      # project = /(?<=projects\/).+/.match(path)
      # puts Rainbow("    #{project.to_s.gsub(/\/lib.*/, '')}").yellow if project
      path
    end
    puts Rainbow("    none").green if found.empty?
    puts
  end
  
  def self.find_project_dir(location)
    location = File.expand_path(File.dirname(location)) unless File.directory?(location)
    l = Pathname.new(location)
    return l if l.children.find { |c| c.to_s =~ /\/lib$/ }
    return nil if l.to_s =~ /^(\/|\\)$/ # hopefully this stops us at the root dir
    find_project_dir(l.parent)
  end
  private_class_method :find_project_dir
end
