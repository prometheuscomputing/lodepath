require 'pathname'
module RelativeFileTools
  def self.relative_to_nth_caller(relative_path_components, caller_depth = 1)
    file = nth_calling_file(caller_depth + 1)
    case file
    when '(irb)' # When calling relative_to_nth_caller from irb, use PWD as dir
      dir = ENV['PWD']
    when /\A\((.*)\)/ # Raise error if not a valid file path
      raise LoadError, "relative is called in #{$1}"
    else # Normal file path
      dir = File.dirname(file)
    end
    File.expand_path(File.join(*relative_path_components), dir)
  end

  # Returns the filename of the caller at the given depth in the stack
  # caller_depth = 1 will return the file that calls nth_calling_file
  def self.nth_calling_file(caller_depth = 1)
    # caller.each_with_index do |c, i|
    #   puts "#{i} -- " + Pathname.new(c.split(/:\d/, 2).first).absolute?.to_s + ':::' + c
    # end
    # puts "CD: #{caller_depth - 1}"
    # cf = caller[caller_depth - 1].split(/:\d/, 2).first
    # puts "CF: #{cf}"
    # pn = Pathname.new(cf)
    # puts "PN: #{pn}"
    # pn
    # puts "Here is nth caller"; puts "#{fn.class}: #{fn}";puts 'END'
    # fn
    Pathname.new(caller[caller_depth - 1].split(/:\d/, 2).first)
  end
  
  def self.ruby_files(path = '', recurse = false)
    # puts "path = #{path}"
    recurse ? Pathname(path).glob("**/*.rb") : Pathname(path).glob("*.rb")
  end
  
  def self.pass_dir_files_to_block(path = nil, recurse = false, caller_depth)
    return if caller.to_s =~ /pass_dir_files_to_block/ # prevent loops
    this_file    = Pathname.new(__FILE__)
    calling_file = RelativeFileTools.nth_calling_file(caller_depth)
    path ||= calling_file.dirname
    paths = RelativeFileTools.ruby_files(path, recurse)
    paths.each do |pn|
      # Do not re-require this file or the calling file
      next if pn == calling_file || pn == this_file
      yield(pn) if block_given?
    end
  end
  
  # Useful during developer testing
  # def ruby_files(path = nil, recurse = false)
  #   RelativeFileTools.ruby_files(path, recurse)
  # end
  
  # Returns an absolute path, given a path relative to the file containing the code that sends this message.
  # Guts shamelessly stolen from http://rubygems.org/gems/require_relative
  def relative(*relative_path_components)
    RelativeFileTools.relative_to_nth_caller(relative_path_components, 2)
  end

  # Require all ruby files directly within a directory. Recursion optional.
  def require_dir(path = nil, recurse = false)
    RelativeFileTools.pass_dir_files_to_block(path, recurse, 3) { |pn| require pn }
  end

  # Load all ruby files directly within a directory. Recursion optional.
  def load_dir(path = nil, recurse = false)
    RelativeFileTools.pass_dir_files_to_block(path, recurse, 3) { |pn| load pn }
  end
end
class Object; include RelativeFileTools; end
