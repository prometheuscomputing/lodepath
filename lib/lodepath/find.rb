module LodePath
  class Error < StandardError; end
    
  # Presumes the receiver specifies a '/' delimited path relative to one of the entries on $LOAD_PATH.
  # Returns an absolute path that points to the first encounted directory specified the receiver.
  # type can be:
  #   * :file
  #   * :directory
  #   * :either
  # mode can be:
  #   * :strict - an exception is raised if the file or directory cannot be found
  #   * :lax - returns nil if no file or directory can be found
  def self.find(sub_path, type=:file, mode=:strict)
    $:.each do |root|
      # puts "     #{root}"
      path = File.join(root, sub_path)
      next unless File.exist?(path)
      abs = File.absolute_path(path)
      case type
        when :either; return abs
        when :file; return(abs) if File.file?(abs)
        when :directory, :dir;  return(abs) if File.directory?(abs)
        else raise Error.new("Unexpected type: expected one of [:file, :directory, :either], got #{type.inspect}")
      end
    end
    case mode
      when :strict; raise Error.new("Failed to find #{type} #{self.inspect} on $LOAD_PATH, and mode is :strict (you could use :lax instead).")
      when :lax; return nil
      else raise Error.new("Unexpected type: expected :file or :dir, got #{type.inspect}")
    end
  end
  
end