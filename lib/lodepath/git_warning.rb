probable_launch_project_dir = caller.reverse.find { |path| path =~ /projects/ }
if probable_launch_project_dir
  pd = LodePath.send(:find_project_dir, probable_launch_project_dir)
  gitignore = File.join(pd, '.gitignore')
  if File.exist?(gitignore)
    gi = File.read(gitignore)
    unless gi =~ /\*_lo\*path.y\*/ || gi =~ /\w\w*(?:lode|load)_?path.ya?ml$/
      Find.find(pd) do |f|
        if f =~ /\w\w*(?:lode|load)_?path.ya?ml$/
          puts Rainbow('!').red.blink + Rainbow('You should add ').red + Rainbow('*?lo*path.y*').yellow + Rainbow(' to your .gitignore file to prevent committing your personal loadpath amendments to the git repository').red + Rainbow('!').red.blink
          break
        end
      end
    end
  end
end