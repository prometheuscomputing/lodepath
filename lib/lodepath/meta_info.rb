module LodePath
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb

  # Required
  GEM_NAME = 'lodepath'
  VERSION  = '0.2.3'
  SUMMARY  = 'Commonly used functionality for manipulating $LOAD_PATH'
  AUTHORS  = ['Michael Faughn']
  EMAILS   = ['michael.faughn@nist.gov']
  HOMEPAGE = 'https://gitlab.com/prometheuscomputing/lodepath'
  DESCRIPTION = 'Commonly used functionality for manipulating $LOAD_PATH (and opening installed gems in an editor).'
  
  LANGUAGE = :ruby
  LANGUAGE_VERSION = ['>= 2.7']
  
  RUNTIME_VERSIONS = {
    :mri => ['~> 2.7'],
  }
  
  DEPENDENCIES_RUBY  = { :rainbow => '' }
  DEVELOPMENT_DEPENDENCIES_RUBY  = { :rspec => '>= 3' } 
end
